# Copyright 2021 Catalyst Cloud Limited
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

from odooclient.common import BasicManager


class OpenStackVolumeDiscountRangeManager(BasicManager):

    def get_volume_discount(self, total, group_id=False):
        ranges = self.list(read=True, customer_group=group_id)
        found_ranges = []
        for vol_range in ranges:
            if total < vol_range['min']:
                continue
            if vol_range['use_max'] and total >= vol_range['max']:
                continue

            found_ranges.append(vol_range)
        
        if not found_ranges:
            return None
        if len(found_ranges) != 1:
            # TODO(adriant): make this a real named exception later...
            raise Exception("We found more than 1 applicable range?! What do?!")
        return found_ranges[0]
