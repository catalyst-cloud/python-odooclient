# Copyright 2021 Catalyst Cloud Limited
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

from odooclient.common import BasicManager


class OpenStackProjectManager(BasicManager):

    def get_by_os_id(self, project_id):
        projects = self.list(
            [('os_id', '=', project_id)], read=True)

        if len(projects) == 0:
            return None

        project = projects[0]
        return project
